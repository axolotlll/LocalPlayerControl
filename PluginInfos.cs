﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LocalPlayerControl
{
    public class PluginInfos
    {
        public const string PLUGIN_ID = "com.gitlab.axolotlll.LocalPlayerControl";
        public const string PLUGIN_NAME = "LocalPlayerControl";
        public const string PLUGIN_VERSION = "1.0.0";
    }
}
