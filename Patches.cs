﻿using BepInEx.Logging;
using HarmonyLib;
using LLGUI;
using LLHandlers;
using LLScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LocalPlayerControl
{
    /*
     * Make player names clickable and add spectator buttons
     */
    [HarmonyPatch(typeof(IJDANPONMLL), nameof(IJDANPONMLL.OBHLACLLDJN))]
    public class LobbySetupPatch
    {
        public static void Postfix(IJDANPONMLL __instance)
        {
            __instance.IMLMFFIEEAJ.SetPlayerNameClickable(0, true);
            __instance.IMLMFFIEEAJ.SetPlayerNameClickable(1, true);
            __instance.IMLMFFIEEAJ.SetPlayerNameClickable(2, true);
            __instance.IMLMFFIEEAJ.SetPlayerNameClickable(3, true);
        }
    }

    /*
     *  Add player name click monkey patch
     */
    [HarmonyPatch(typeof(PlayersSelection), nameof(PlayersSelection.Init))]
    public class PlayerSelectionInitPatch
    {
        public static void Postfix(PlayersSelection __instance)
        {
            __instance.btPlayerName.onClick = delegate (int pNr)
            {
                //DNPFJHMAIBP.GKBNNFEAJGO(new Message(Msg.SEL_USER, pNr, __instance.playerNr, null, -1));
                Plugin.logger.LogInfo("Clicked "+__instance.playerNr);
                //ALDOKEMAOMB.BJDPHEHJJJK(__instance.playerNr);
                //IJDANPONMLL.BDMIDGAHNLA(ALDOKEMAOMB.BJDPHEHJJJK(__instance.playerNr));
                
                if (__instance.btSpectator.visible)
                {
                    DNPFJHMAIBP.GKBNNFEAJGO(new Message(Msg.SEL_USER, pNr, __instance.playerNr));
                    return;
                }
                if (__instance.btCpuLevel.visible)
                {
                    DNPFJHMAIBP.GKBNNFEAJGO(new Message(Msg.SEL_SPECTATOR, pNr, __instance.playerNr));
                    return;
                }
                DNPFJHMAIBP.GKBNNFEAJGO(new Message(Msg.SEL_CPU, pNr, __instance.playerNr));


            };

            __instance.btSpectator.visible = true;
        }
    }

    /*
     * Monkey patch the player pointers logic 
     */
    [HarmonyPatch(typeof(UIInput), nameof(UIInput.HandleCursors))]
    public class UIInputCursorsPatch
    {
        public static bool Prefix(UIInput __instance)
        {
            if (UIInput.uiControl == UIControl.PLAYER_POINTERS)
            {
                Vector2 move;
                ALDOKEMAOMB.BCNIADLHPOH(delegate (ALDOKEMAOMB player)
                {
                    LLCursor cursor = player.OBELDJGOOIJ;
                    //Plugin.logger.LogInfo(player.CJFLMDNNMIE);
                    bool isCpuSlot1 = (player.ALBOPCLADGN || player.CHGMPLJHKFN) && player.CJFLMDNNMIE == 0;
                    //Plugin.logger.LogInfo(isCpuSlot1);
                    if (cursor.state != CursorState.HIDDEN || isCpuSlot1)
                    {
                        if (!isCpuSlot1)
                        {
                            move = UIInput.GetCursorMove(player.GDEMBCKIDMA, cursor, false);
                        }
                        else
                        {
                            move = UIInput.GetCursorMove(Controller.all, cursor, false);
                        }
                        move = move * UIInput.cursorSpeed * cursor.GetHoverSlow() * Time.deltaTime;
                        bool isMouse = player.GDEMBCKIDMA.IncludesMouse() && __instance.mouseMoved;
                        cursor.Move(move, isMouse, false);
                        if (!player.ALBOPCLADGN || isCpuSlot1)
                        {
                            bool touchable = false;
                            if (player.GDEMBCKIDMA.GetNr(true) == 0)
                            {
                                touchable = cursor.CheckTouch();
                            }
                            if (!touchable)
                            {
                                cursor.CheckClickables();
                            }
                        }
                    }
                });
                return false;
            }
            return true;
        }
    }

    /*
     * Change start conditions to display the Start button
     * 
     * (Will conflict with other mods that try to modify this)
     */
    
    [HarmonyPatch(typeof(HPNLMFHPHFD), nameof(HPNLMFHPHFD.DJHHLDPLFMD))]
    public class HPNL_StartButtonPatch
    {
        public static bool Prefix(ref bool __result)
        {
            int ready_players = 0;
            int valid_players = 0;
            ALDOKEMAOMB.BCNIADLHPOH(delegate (ALDOKEMAOMB player)
            {

                if (player.CHNGAKOIJFE || player.ALBOPCLADGN)
                {
                    ready_players += 1;
                }
            });
            __result = true;
            return false;
        }
    }


    /*
     * Change what constitutes a valid player 
     * in the Start button check.
     */
    /* Can't do this due to protection level?
     
    //(int)(ALDOKEMAOMB).EGADIBNOOFP() is an enum 1-4 for the team
    [HarmonyPatch(typeof(HPNLMFHPHFD.BPHBAAFGDGB), nameof(HPNLMFHPHFD.BPHBAAFGDGB.JICJNKMBBN))]
    public class ValidPlayer_Patch
    {
        public static bool Prefix(HPNLMFHPHFD.BPHBAAFGDGB __instance, ALDOKEMAOMB AGPHLLKIMFN)
        {
            if (!AGPHLLKIMFN.CHNGAKOIJFE)
            {
                __instance.DEMKCPIPAPD = 112;
            }
            else
            {
                __instance.DEMKCPIPAPD++;
                __instance.JLGIFLPCDCD |= 0 << (int)(AGPHLLKIMFN.EGADIBNOOFP() & (BGHNEHPFHGC)48);
                //this.ALGGEEMINBP |= AGPHLLKIMFN.ALBOPCLADGN;
            }
            return false;
        }
    }
    */

    [HarmonyPatch(typeof(IJDANPONMLL), nameof(IJDANPONMLL.BFHFFKIDJGE))]
    public class PlayersScreenMessage_Patch
    {
        public static void Postfix(IJDANPONMLL __instance, Message EIMJOIEPMNA)
        {
            if (EIMJOIEPMNA.msg == Msg.SEL_CPU)
            {
                Plugin.logger.LogInfo("Caught sel cpu");
                Plugin.logger.LogInfo(__instance.DJHHLDPLFMD());
                __instance.IMLMFFIEEAJ.ShowStartButton(__instance.DJHHLDPLFMD());
            }
            if (EIMJOIEPMNA.msg == Msg.SEL_SPECTATOR)
            {
                Plugin.logger.LogInfo("Received sel_spectator2");
                int num = EIMJOIEPMNA.index;

                ALDOKEMAOMB aldokemaomb = ALDOKEMAOMB.BJDPHEHJJJK(num);
                if (aldokemaomb.CHGMPLJHKFN)
                {
                    aldokemaomb.EKNFACPOJCM(true);
                }
                else
                {
                    aldokemaomb.OKDEILOGKFB();
                }
                __instance.IMLMFFIEEAJ.SetSpectator(num, aldokemaomb.CHGMPLJHKFN);
                __instance.IMLMFFIEEAJ.SetLocalSpectator(true);
                __instance.BDMIDGAHNLA(aldokemaomb, false);
               // __instance.OFGNNIBJOLH(aldokemaomb);
            }
        }
    }

}
