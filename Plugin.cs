﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using LLBML.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LocalPlayerControl
{
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        public static Plugin instance;
        public ConfigEntry<bool> isModEnabled;
        public static ManualLogSource logger;
        void Awake()
        {
            instance = this;
            logger = this.Logger;
            var harmony = new Harmony(PluginInfos.PLUGIN_ID);
            harmony.PatchAll();

            InitConfig();
        }

        void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info);
        }
        void InitConfig()
        {
            isModEnabled = Config.Bind<bool>("Toggles", "Enabled", true, "Enables / disables the mod.");
        }
    }
}
